# MetaSimilarity #

Implements a strategy for finding sets of similar entities, where there are many potential similarity metrics to consider (i.e., a highly parallel multigraph).

Relies on Power Iteration Clustering (PIC) as provided by Spark/Mllib.

### Building

`sbt package`

### Running

After building, and with Spark located at `$SPARK_HOME`, run with:

`$SPARK_HOME/bin/spark-submit --master "local" --class MetaSimilarity target/scala-2.11/metasimilarity_2.11-1.0.jar 25 3`

This produces three representative clusterings of three classes, using 25 initial views of the similarity parameter space.

### Issues

Currently runs as a fully-contained script; needs to be refactored for use with independant data/metrics.
