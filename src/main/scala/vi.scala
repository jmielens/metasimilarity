import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.mllib.clustering.PowerIterationClustering
import org.apache.spark.mllib.clustering.PowerIterationClustering.Assignment
import java.io._

object MetaSimilarity extends App {

	val r = scala.util.Random
	type CiteSpacePoint = List[Int]
	type SingleWeightedGraph = List[(Long,Long,Double)]
	var K = 100
	var k = 2

	// Data Generation
	val names = List(("Joe","Brown"),("Joe","Yellow"),("Steve","Blue"),("Sue","Green"),
		("Mary","Blue"),("Fred","Black"),("Tim","Grey"),("Nancy","Red"),
		("Becky","Blue"),("Jeff","Black"),("Steve","Pink"),("Nancy","Green"))
	val authors = names.map{case (f,l) => 
		Author(f,l,r.nextInt(100),
		List(r.nextInt(200),r.nextInt(200),r.nextInt(200),r.nextInt(200),r.nextInt(200)),
		List(r.nextInt(200),r.nextInt(200),r.nextInt(200),r.nextInt(200),r.nextInt(200)))
	}

	// Factor these externally
	def citeDist(u: CiteSpacePoint, v: CiteSpacePoint): Double = {
	  math.sqrt(u.zip(v).map{ case(i,j) => math.pow(i-j,2) }.sum)
	}
	def norm(u: List[Double]): List[Double] = {
	  val v = u.map(x => 0)
	  val n = math.sqrt(u.zip(v).map{ case(i,j) => math.pow(i-j,2) }.sum)

	  return u.map(x => x/n)
	}

	def VI(assoc: Array[Array[Int]]): Double = {
		def getCol(n: Int, a: Array[Array[Int]]) = a.map{_(n)}

		var vi = 0.0
		val N = assoc.map(_.sum).sum.toDouble

		val k = assoc.length
		val k_ = assoc(0).length

		// VI(C,C') = H(C) + H(C') - 2I(C,C')
		// H(C)
		vi -= (0 until k).map{ K =>
			val pk = assoc(K).sum/N
			pk * scala.math.log(pk)
		}.sum

		// H(C')
		vi -= (0 until k_).map{ K =>
			val pk = getCol(K,assoc).sum/N
			pk * scala.math.log(pk)
		}.sum
		
		// I(C,C')
		var I = 0.0

		(0 until k).foreach{ K =>
			val pk = assoc(K).sum/N
			(0 until k_).foreach{ K_ =>
				val p = assoc(K)(K_)/N
				val pk_ = getCol(K_,assoc).sum/N
				val i = p * scala.math.log(p/(pk*pk_))
				if (i.isNaN) {

				} else {
					I += i
				}
			}
		}

		vi -= 2*I

		return if (vi < 0.0) 0.0 else vi
	}

	implicit class Crossable[X](xs: Traversable[X]) {
		def cross[Y](ys: Traversable[Y]) = for { x <- xs; y <- ys } yield (x, y)
	}

	def buildAssocMatrix(u:Array[Assignment], v:Array[Assignment]): Array[Array[Int]] = {
		val k = u.map(_.cluster).max + 1
		val k2 = v.map(_.cluster).max + 1
		val n = u.length

		var assoc = scala.Array.ofDim[Int](k,k2)
		for (x <- 0 until n) {
			val i = u.filter(_.id == x).head.cluster
			val j = v.filter(_.id == x).head.cluster

			assoc(i)(j) += 1
		}
		return assoc
	}

	def buildHardSimMatrix(assignment:Array[Assignment]): Array[Array[Int]] = {
		val n = assignment.length
		var sim = scala.Array.ofDim[Int](n,n)

		val clusters = assignment.map(x => (x.id,x.cluster)).sortBy(_._1).map(_._2)

		for (i <- 0 until n; j <- 0 until n) {
			sim(i)(j) = if (clusters(i) == clusters(j)) 1 else 0
		}
		return sim
	}

	case class Author(first: String, last: String, age: Int, citedBy: CiteSpacePoint, cites: CiteSpacePoint)
	case class MultiWeightEdge(src: Long, dst: Long, weights: List[Double])

	// Age, citedBy, cites
	def getMultiEdge(ai: Long, bi: Long): MultiWeightEdge = {
		val a = authors(ai.toInt)
		val b = authors(bi.toInt)
		val ageSim = scala.math.abs(a.age-b.age)
		val citedBySim = citeDist(a.citedBy,b.citedBy)
		val citesSim = citeDist(a.cites,b.cites)

		MultiWeightEdge(ai,bi,List(ageSim,citesSim,citesSim))
	}

	def generateSingleWeightedGraph(mwe: List[MultiWeightEdge]): SingleWeightedGraph = {
		val alpha = norm(List(r.nextDouble,r.nextDouble,r.nextDouble))

		mwe.map(x => (x.src,x.dst,alpha(0)*x.weights(0)+alpha(1)*x.weights(1)+alpha(2)*x.weights(2)))
	}

	val conf = new SparkConf().setAppName("MetaSimilarity")
	val sc = new SparkContext(conf)

	authors.foreach(println)

	// Base Graph Construction
	val V = (0 until authors.length).toList
	val E = (V cross V).filter{ case (u,v) => u != v }.map{ case (u,v) =>
		getMultiEdge(u,v) // TODO: not twice the work...
	}.toList

	// Construct Multiple Graphs after converting to single edge with random parameters
	K = args(0).toInt
	k = args(1).toInt
	val Es = (0 until K).map{i => sc.parallelize(generateSingleWeightedGraph(E))}

	// Cluster each graph from the sampled parameter space
	val assignments = Es.map{ graphRdd =>
		val model = new PowerIterationClustering()
		  .setK(k)
		  .setMaxIterations(100)
		  .setInitializationMode("degree")
		  .run(graphRdd)

		val clusters = model.assignments.collect().groupBy(_.cluster).mapValues(_.map(_.id))
		val assignments = clusters.toList.sortBy { case (k, v) => v.length }
		val assignmentsStr = assignments
		  .map { case (k, v) =>
		    s"$k -> ${v.sorted.mkString("[", ",", "]")}"
		  }.mkString(", ")
		val sizesStr = assignments.map {
		  _._2.length
		}.sorted.mkString("(", ",", ")")
		//println(s"Cluster assignments: $assignmentsStr\ncluster sizes: $sizesStr")
		//println("")
		model.assignments.collect()
	}


	// Take each Sampled Clustering as a Node in a Meta Graph
	// Edge weights are VI(C,C')
	val VC = (0 until K).toList
	val EC:List[(Long,Long,Double)] = (VC cross VC).filter{ case (u,v) => u != v }.map{ case (u,v) =>
		val assoc = buildAssocMatrix(assignments(u),assignments(v))
		(u.toLong, v.toLong, VI(assoc))
	}.toList

	val sampledRDD = sc.parallelize(EC)

	// Cluster Meta Graph
	val modelMeta = new PowerIterationClustering()
		  .setK(k)
		  .setMaxIterations(100)
		  .setInitializationMode("degree")
		  .run(sampledRDD)

	val clustersMeta = modelMeta.assignments.collect().groupBy(_.cluster).mapValues(_.map(_.id))
	val assignmentsMeta = clustersMeta.toList.sortBy { case (k, v) => v.length }
	val assignmentsMetaStr = assignmentsMeta
	  .map { case (k, v) =>
	    s"$k -> ${v.sorted.mkString("[", ",", "]")}"
	  }.mkString(", ")
	val sizesMetaStr = assignmentsMeta.map {
	  _._2.length
	}.sorted.mkString("(", ",", ")")
	//println(s"Meta Cluster assignments: $assignmentsMetaStr\ncluster sizes: $sizesMetaStr")
	//println("")


	// Average each Meta Cluster to a Representative Cluster (CSPA)
	(0 until k).map{ metaClusterID =>
		val clusterings = assignments.zipWithIndex.filter{ case (a, i) => clustersMeta(metaClusterID).contains(i) }.map(_._1)
		val hardAssoc = clusterings.map{ assignment =>
			buildHardSimMatrix(assignment)
		}

		val n = clusterings(0).length
		var avgSim = scala.Array.ofDim[Double](n,n)

		for (i <- 0 until n; j <- 0 until n) {
			avgSim(i)(j) = hardAssoc.map(x => x(i)(j)).sum.toDouble/n.toDouble
		}
		/*
		val pw = new PrintWriter(new File(metaClusterID+".tsv"))
		pw.write(avgSim.map(_.mkString("\t")).mkString("\n"))
		pw.close()
		*/

		val EC:List[(Long,Long,Double)] = ((0 until n) cross (0 until n)).filter{ case (u,v) => u != v }.map{ case (u,v) =>
			(u.toLong, v.toLong, avgSim(u)(v))
		}.toList

		val sampledRDD = sc.parallelize(EC)


		// Cluster Rep Graph
		val repModel = new PowerIterationClustering()
			  .setK(k)
			  .setMaxIterations(100)
			  .setInitializationMode("degree")
			  .run(sampledRDD)
		val clustersRep = repModel.assignments.collect().groupBy(_.cluster).mapValues(_.map(_.id))
		val assignmentsRep = clustersRep.toList.sortBy { case (k, v) => v.length }
		val assignmentsRepStr = assignmentsRep
		  .map { case (k, v) =>
		    s"$k -> ${v.sorted.mkString("[", ",", "]")}"
		  }.mkString(", ")
		val sizesRepStr = assignmentsRep.map {
		  _._2.length
		}.sorted.mkString("(", ",", ")")
		println(s"Rep Cluster assignments: $assignmentsRepStr\ncluster sizes: $sizesRepStr")
		println("")
	}

}
